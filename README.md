# My PCI passthrough configuration

## Introduction

This is a repository containing information and tools I've gathered while setting up PCI passthrough on my system to be able to pass host PCIe devices through to a KVM & QEMU guest, using libvirt as a configuration helper.

### For context, here's my specs:

- **CPU:** AMD Ryzen 7 5800X
- **Motherboard:** GIGABYTE X570 AORUS ELITE
- **GPU:** ASUS TUF NVIDIA GeForce RTX 3060 Ti Lite Hash Rate
- **RAM**: 32 GB Corsair LPX DDR4 3200 MHz CL16 memory (4x8 GB)
- **Storage:** Host is an NVMe SSD, guest is using a raw disk image

## Setup

### Enabling IOMMU & virtualisation

In my motherboard's BIOS I can enable IOMMU and AMD SVM. This will be explained in your motherboard's manual. After doing such, adding `amd_iommu=on` and `iommu=pt` to your kernel parameters will ensure that it is active and passthrough is enabled. Some kernels are compiled with IOMMU support enabled by default, but its good to be sure.

### Checking IOMMU groupings

Using the script [check-iommu-groups.sh](scripts/check-iommu-groups.sh) you can generate a list of all the PCI devices on your system, their address, IDs and IOMMU groups. My graphics card inserted into the top 16x slot on my motherboard was placed into its own IOMMU group along with its HDMI audio controller so that's all fine. When studying this, make sure all the PCI devices you want to pass through to your virtual machine do not share their IOMMU groups with devices you do not want to pass through. Otherwise see the section "IOMMU group issues".

### Specifying devices to passthrough early in boot process

The next step is to bind the vfio_pci driver to devices you want to passthrough. I used the follow parameter to specify the PCI IDs which I wanted vfio_pci to bind to: `fio-pci.ids=10de:2489,10de:228b`. It's important to early load the following modules so they bind to the device(s) before other modules: `vfio_pci vfio vfio_iommu_type1 vfio_virqfd`. Regenenerate your initramfs images and bootloader configurations.

### IOMMU group issues

For example, if you wanted to pass a device through to your VM but your IOMMU group looked like the following:

```
IOMMU Group 13:
	01:00.0 Example device 1 [0001]: A device which I'd like to pass through [0123:4567] (rev 01)
	02:00.0 A different device [0002]: A pesky device I do NOT want to pass through [4444:8888] (rev 01)
```

In this example IOMMU group I've created, we have the device "Example device 1" which we *do* want to pass through and a second device "A different device" which we *don't* want to pass through. The problem here is if we try to pass the first device through, we will receive and error saying the IOMMU group is not viable. The solution to this is to either bind `vfio_pci` to "A different device" as well, but this means that the host will not be able to use it either. Often this is undesirable and so people may use PCI ACS overrides instead. See [acs-override.md](acs-override.md).
