# libvirt & QEMU hooks

| Hook folder | Is run when ... |
| --- | --- |
| prepare | beofore a VM is started, before resources are allocated |
| start | before a VM is started, after resources are allocated |
| started | after a VM has started up |
| stopped | after a VM has shut down, before releasing its resources |
| release | after a VM has shut down, after resources are released |

Source: https://passthroughpo.st/simple-per-vm-libvirt-hooks-with-the-vfio-tools-hook-helper/