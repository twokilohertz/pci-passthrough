# Notes on ACS overrides

Using Arch Linux and the linux-zen kernel, which has ACS override support by default, you can just add `pcie_acs_override=downstream,multifunction` as a kernel parameter and *in theory* it should break up as many PCI devices as possible. Source: https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF#Bypassing_the_IOMMU_groups_(ACS_override_patch)

Supposedly, for just separating one device out you can use `pcie_acs_override=id:nnnn:nnnn` as the kernel parameter. It's important to note that you must put *all* the devices in the IOMMU group you wanted to split out into the `pcie_acs_override` parameter.

For example, on my motherboard, I have an IOMMU group which contains one of the USB controllers I'd like to pass through, but has other devices in the same group I do not want to pass through.

```
IOMMU Group 20:
	03:08.0 PCI bridge [0604]: Advanced Micro Devices, Inc. [AMD] Matisse PCIe GPP Bridge [1022:57a4]
	08:00.0 Non-Essential Instrumentation [1300]: Advanced Micro Devices, Inc. [AMD] Starship/Matisse Reserved SPP [1022:1485]
	08:00.1 USB controller [0c03]: Advanced Micro Devices, Inc. [AMD] Matisse USB 3.0 Host Controller [1022:149c]
	08:00.3 USB controller [0c03]: Advanced Micro Devices, Inc. [AMD] Matisse USB 3.0 Host Controller [1022:149c]
```

If I add all of these devices to the ACS override kernel parameter using the following: `pcie_acs_override=id:1022:57a4,id:1022:1485,id:1022:149c,id:1022:149c` it splits them all out into their own IOMMU groups:

```
IOMMU Group 28:
        08:00.1 USB controller [0c03]: Advanced Micro Devices, Inc. [AMD] Matisse USB 3.0 Host Controller [1022:149c]
```

An important thing to note with ACS overrides is we are overriding an *access control service*. PCIe ACS provides the ability for the computer to prevent PCIe devices from accidentally (or maliciously) accessing other devices' memory. If the ACS override we apply is actually just an override over a quirk or oversight in this platform's design and won't actually cause any problems, it's fine. But it is possible to cause problems with ACS override and it should be taken carefully. See: https://vfio.blogspot.com/2014/08/iommu-groups-inside-and-out.html

