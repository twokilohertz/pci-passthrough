#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1 # return error
fi

GUEST_NAME="$1"

# Check hook directories exist
[ ! -d /etc/libvirt/hooks/qemu.d/$GUEST_NAME ] && mkdir -p /etc/libvirt/hooks/qemu.d/GUEST_NAME
[ ! -d /etc/libvirt/hooks/qemu.d/$GUEST_NAME/prepare/begin/ ] && mkdir -p /etc/libvirt/hooks/qemu.d/GUEST_NAME/prepare/begin/
[ ! -d /etc/libvirt/hooks/qemu.d/$GUEST_NAME/start/begin/ ] && mkdir -p /etc/libvirt/hooks/qemu.d/GUEST_NAME/start/begin/
[ ! -d /etc/libvirt/hooks/qemu.d/$GUEST_NAME/started/begin/ ] && mkdir -p /etc/libvirt/hooks/qemu.d/GUEST_NAME/started/begin/
[ ! -d /etc/libvirt/hooks/qemu.d/$GUEST_NAME/stopped/end/ ] && mkdir -p /etc/libvirt/hooks/qemu.d/GUEST_NAME/stopped/end/
[ ! -d /etc/libvirt/hooks/qemu.d/$GUEST_NAME/release/end/ ] && mkdir -p /etc/libvirt/hooks/qemu.d/GUEST_NAME/release/end/

echo "Copying hook scripts..."

cp ../hooks/prepare/* /etc/libvirt/hooks/qemu.d/$GUEST_NAME/prepare/begin/
cp ../hooks/start/* /etc/libvirt/hooks/qemu.d/$GUEST_NAME/start/begin/
cp ../hooks/started/* /etc/libvirt/hooks/qemu.d/$GUEST_NAME/started/begin/
cp ../hooks/stopped/* /etc/libvirt/hooks/qemu.d/$GUEST_NAME/stopped/end/
cp ../hooks/release/* /etc/libvirt/hooks/qemu.d/$GUEST_NAME/release/end/

echo "Done copying hook scripts!"