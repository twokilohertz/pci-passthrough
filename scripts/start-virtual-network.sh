#!/bin/bash

# Starts the virtual network
# Should be run with root privileges

virsh net-autostart default
virsh net-start default