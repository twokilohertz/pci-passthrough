#!/bin/bash

# Installs and sets up qemu & libvirt
# Assumes a system with pacman & systemd
# Should be run with root privileges

pacman -S qemu libvirt edk2-ovmf virt-manager iptables-nft dnsmasq
systemctl --now enable libvirtd.service
systemctl --now enable virtlogd.socket